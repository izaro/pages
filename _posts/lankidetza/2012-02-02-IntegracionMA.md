---
layout: post
title:  "Integración del Medio Ambiente en la Cooperación"
date:   2012-02-02
categories: es cooperation environment
tags: [es,medio ambiente, cooperación, tesina]
---


Hola lectores y lectoras,

Durante el año pasado estuve realizando el<a href="http://www.hegoa.ehu.es/es/formacion/master_oficial_en_desarrollo_y_cooperacion_internacional" target="_blank" rel="noopener"> Máster de Cooperación y Desarrollo de Hegoa</a>; y como Tesina, decidí tratar el tema delmedio ambiente en la cooperación descentralizada . Me centré en las comunidades autónomas de España y me propuse ver cómo tratan los planes directores de cooperación la inclusión del medio ambiente en sus estrategias. La idea principal de la tesina, era que fuese algo útil y que todas las personas que trabajan en los temas de medio ambiente y cooperación pudiesen aprovechar para su trabajo, investigaciones, o lo que fuese necesario. La verdad es que me horrorizaba la idea de dedicar tanto tiempo a un trabajo para que estuviese encerrado en una carpeta absorbiendo polvo electrónico.
<p style="text-align:left;"><img class="aligncenter" src="http://izaroblog.files.wordpress.com/2012/02/ideas.jpg?w=300" alt="" width="346" height="230" /></p>
Tras realizar el análisis de los distintos planes de cooperación, a continuación os pongo las principales conclusiones a las que llegué:
<ul>
 	<li>Existe un alto grado de alineación entre los planes autonómicos y el "<a href="http://www.maec.es/es/MenuPpal/CooperacionInternacional/Publicacionesydocumentacion/Documents/lineasmaestras09-12_Es.pdf" target="_blank" rel="noopener">Plan Director Español de Cooperación 2009-2012</a>” en relación al medio ambiente, así como con los principales acuerdos alcanzados en <a href="http://izaroblog.wordpress.com/wp-admin/www.oecd.org/dataoecd/53/56/34580968.pdf" target="_blank" rel="noopener">París</a> y <a href="http://www.google.es/url?sa=t&amp;rct=j&amp;q=acuerdos%20de%20accra&amp;source=web&amp;cd=4&amp;ved=0CEEQFjAD&amp;url=http%3A%2F%2Fwww.oecd.org%2Fdataoecd%2F58%2F19%2F41202043.pdf&amp;ei=RrgqT4qLCIHpOZ3-1YgO&amp;usg=AFQjCNEuWIX3FQWaPzlekC_CBQs_KI0z6A&amp;sig2=eIW5ejAoadLwJnK2JPC8uQ&amp;cad=rja" target="_blank" rel="noopener">Accra</a>.</li>
 	<li>La mayoría de los planes autonómicos tratan de una forma<strong> transversal</strong> el medio ambiente, y se encuentra incluido dentro de las estrategias sectoriales.</li>
 	<li>La coordinación es un concepto muy trabajado en los planes, pero con el fin de evitar casos de "anticooperación" la <strong> coherencia</strong> entre las diferentes políticas no lo es tanto. Únicamente he encontrado un caso (Cataluña) en el que se proponen medidas para que las políticas de cooperación sean conocidas y se tengan en cuenta por el resto de políticas autonómicas.</li>
 	<li>Aunque en todos los planes el medio ambiente se encuentre incluido dentro de la estrategia, se va <strong> difuminando</strong> a la hora de incluirlo en los proyectos o líneas de trabajo más específicas. A nivel de proyectos es difícil ver proyectos con una gran carga ambiental.</li>
 	<li>Las estrategias de más importancia dentro del paraguas de "medio ambiente" son los de <strong> Agua y Saneamiento</strong> . El Cambio climático en cambio no es un área prioritaria dentro de las estrategias.</li>
</ul>
Si quieres echarle un vistazo a la tesina en más profundidad, puedes descargarla desde aquí, estaré encantado de recibir comentarios y sugerencias sobre el tema!!

<a href="https://izaroblog.files.wordpress.com/2012/02/integracic3b3n-del-medio-ambiente-en-la-polc3adtica-de-cooperacic3b3n-descentralizada.pdf"><img class=" wp-image-2945" src="https://izaroblog.files.wordpress.com/2012/02/800px-libreoffice_icon_3.3.1_48_px.svg_.png?w=250" alt="" width="120" height="144" /></a> 
Descargar tesina

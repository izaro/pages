---
layout: post
title:  "Derecho Humano al Medio Ambiente"
date:   2013-09-23
categories: es environment
tags: [es, Derecho Humano al Medio Ambiente, Unesco]
---

<span style="font-size:large;font-family:'Ubuntu Light';">Hace poco se ha publicado el libro <a title="Ecos serpiente" href="http://www.unescoetxea.org/base/berriak.php?hizk=es&amp;id_atala=1&amp;id_azpiatala=1&amp;zer=orokorrean&amp;nor=1139" target="_blank">“Ecos de la Serpiente, Reflexiones desde el Consejo de Derechos Humanos</a>”. Es una obra que surge tras<a title="¿Cómo pueden participar las ONG en el Consejo de Derechos&nbsp;Humanos?" href="http://izaroblog.com/2012/10/11/como-pueden-participar-las-ong-en-el-consejo-de-derechos-humanos/" target="_blank"> la estancia que disfrutamos en Ginebra</a>, gracias a una beca de <a title="Unesco Etxea" href="http://unescoetxea.org/" target="_blank">Unesco Etxea</a>.</span>


<p style="text-align:center;"><a href="http://www.unescoetxea.org/dokumentuak/ecos_serpiente2.pdf"><img class="aligncenter size-full wp-image-1357" src="http://izaroblog.files.wordpress.com/2013/09/berria_ecosserpiente21139.jpg" alt="Berria_ecosserpiente21139" width="200" height="283"></a>
<p>&nbsp;<span style="font-family:'Ubuntu Light';"><span style="font-size:large;">
  
En el artículo que escribo para la publicación hablo sobre el “Derecho Humano al Medio Ambiente”, explicando <strong>cómo surgió la idea</strong>, <strong>qué relaciones tiene con el resto de derechos humanos</strong>, <strong>qué pasos se han dado</strong> hasta el momento y la <strong>situación en la que se encuentra actualmente</strong>.<br>
Para terminar menciono las principales amenazas con las que se encuentra la propuesta y las que se va a encontrar terminando con unas conclusiones.</span></span></p>
<blockquote><p><span style="font-family:'Ubuntu Light';"><span style="font-size:large;">Un largo camino por recorrer</span></span></p></blockquote>
<p><span style="font-size:large;font-family:'Ubuntu Light';">Además de mi articulo podéis leer también las de mis compañeras<strong> Silvia Felipe, Cristina Fernández, Sarai Martín, Natalia Rodriguez, Aimar Rubio</strong> y <strong>Eva Sánchez.</strong></span></p>
<p><span style="font-size:large;font-family:'Ubuntu Light';">Si queréis echarle un vistazo al artículo&nbsp;“Derecho Humano al Medio Ambiente”podéis pinchar más abajo:</span></p>
<p><span style="font-family:'Ubuntu Light';"><span style="font-size:large;">&nbsp;</span></span></p>
<p><a href="http://izaroblog.files.wordpress.com/2013/11/derecho-humano-al-medio-ambiente_1.pdf"><img class=" wp-image-1361      " src="http://izaroblog.files.wordpress.com/2013/09/libreoffice_4-0_main_icon-svg.png" alt="Artículo “Derecho Humano al Medio Ambiente”" width="205" height="205"></a> Artículo “Derecho Humano al Medio Ambiente”</p>
<p><span style="font-family:'Ubuntu Light';"><span style="font-size:large;">&nbsp;</span></span></p>

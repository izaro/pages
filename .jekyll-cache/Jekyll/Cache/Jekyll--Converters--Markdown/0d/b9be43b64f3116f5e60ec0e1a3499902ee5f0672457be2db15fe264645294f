I"�<p>Gaurkoan gomendio batekin natorkizue. Lehengoan <a href="https://miren.bz/">Miren Berasategi</a>k bere blogean itzuli zuen “Zettelkasten” izeneko teknika bati buruzko artikulua: <strong><a href="https://miren.bz/post/zettelkasten/">Zettelkasten — Jakintsu alemaniar baten produktibitate sinesgaitza</a></strong></p>

<p>Ez dut hemen errepikatuko zertan datzan, ze artikulu hortan bertan primeran adierazita dago zein den teknika, nondik datorren eta zer egin dezakezuen berau erabiltzeko, Mirenek bikain egin du eta! ;)</p>

<p>Ni hasi naiz produktibitate tresna hau probatzen eta oso gomendagarria dela esango nizueke, ideiak antolatzeko eta gehienbat haiek haritzeko lagungarria egiten zaidalako gehienbat. Dudan esperientzia gutxiarekin esango dizuet hasieran zailtxo egiten dela hastea, eta ondo-ondo sistema ulertzea baina behin pausu batzuk emanda dituzunean, nahiko nabaria da lortzen duzun hobekuntza.</p>

<p><img src="https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/postimages/kasten.png" alt="Kasten" /></p>

<p>Hori bai, gero bakoitzak ikusi behar du nola jorratu eta ze truko txiki erabili gauzak gordetzeko garaian, testu formatuak, etab. 
Nire aukeraketa kontatuko dizuet, lagungarria izan daitekeelakoan.</p>

<ul>
  <li>Testu formatua: <a href="https://eu.wikipedia.org/wiki/Markdown">Markdown</a>, hau da “izena.md” formatua erabiliko dut zettel edo ohartxoentzako. Oso sinplea da, baina estekak eta testua nabarmentzeko aukerak uzten dizkit. Oso pisu gutxi dute, eta edonon zaudela eta duzun ordenagailua izanda ere irakurtzeko erraztasuna eskaintzen dizu.</li>
  <li>Sinkronizaziorako <a href="https://eu.wikipedia.org/wiki/Nextcloud">Nextcloud</a> erabiltzen dut, baina edozein beste programa ere erabil dezakezue.</li>
  <li>Oharren kode bezala batzuk eguna edota hizkiak erabiltzea gomendatzen dute. Nik akronimo ohartxo bat egin dut, gaiaren arabera sailkatzeko. Ez du garrantzi askorik, baina “A1.0. izena” bezalako sistema errazagoa egiten zitzaidan kudeatzeko “202104271233. izena” baino. Nire kasuan adibidez, <strong>E</strong> aurrizkia duten ohartxoak <em>Energia</em>rekin izango dute erlazioa eta <strong>P</strong> dutenak <em>Politika</em>-rekin. Baina tira, ziur egongo direla <em>energia</em> eta <em>politika</em>rekin erlazioa duten ohartxoak eta aukeraketa egin beharko da! Azken finean aurrizkia erabiltzeak ohartxoak ez errepikatu eta azkarrago bilatzeko balio behar digu!</li>
</ul>

<p><img src="https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/18f223559f4046a1b07a141009348f160613822d/_images/postimages/akronimoak.png" alt="Akronimoak" /></p>

<p>Esandakoa… gogoa baduzu proba ezazu, merezi du eta!</p>

<p><strong>Eguneraketa</strong>:
<a href="@miren@mastodon.eus">Miren</a>-en eta <a href="@okerreko@mastodon.eus">Jokin</a>-en iruzkinak jaso eta gero <strong><a href="https://obsidian.md/">Obsidian</a></strong> programa probatzea erabaki dut, eta egia esan oso ondo dabil eta hobekuntza politak dakartza. Niretzako onena, nahi ezkero estekak automatikoki eguneratzen dizkizula, eta ohartxo bate eta bestearen arteko erlazioa egiteko [[]] formatua besterik ez da erabili behar! Gainera, nodoak era grafikoan ere erakusten ditu, eta oso erabilgarria izan daiteke, “zettel”-ak bilatzeko garaian. Merezi du probatzeak!</p>

<p><img src="https://raw.githubusercontent.com/IzaroBlog/IzaroBlog.github.io/main/_images/postimages/ObsidianIrudia.png" alt="Obsidian" /></p>

:ET
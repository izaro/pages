---
layout: post
title:  "Integrando el medio ambiente en el marco lógico"
date:   2012-03-05
categories: es cooperation
tags: [es, cooperación, medio ambiente, marco lógico, proyectos]
---
<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">Siguiendo con el tema del medio ambiente y la cooperación al desarrollo, a continuación os propongo </span></span></span><span style="font-family:Ubuntu;"><span style="font-size:medium;">revisar un estudio publicado recientemente [Manual para la integracion del medio ambiente en la cooperacion al desarrollo](https://www.miteco.gob.es/es/ceneam/recursos/materiales/integracion-ma-proyectos-desarrollo.aspx) realizado por <a href="http://www.fundacion-biodiversidad.es/" target="_blank" rel="noopener">Fundación Biodiversidad</a> e <a href="http://www.fundacion-ipade.org/" target="_blank" rel="noopener">IPADE</a>.</span></span>
<div>

<span style="font-size:medium;font-family:Ubuntu;">La propuesta que se hace en este manual trata de integrar el medio ambiente en proyectos de cooperación para el desarrollo. El manual, ha sido elaborado a través de un esfuerzo teórico que tiene los siguientes dos limitantes:</span>
<ol>
 	<li style="text-align:left;"><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"><em>Las propuestas en él incluidas no supusiesen una sobrecarga de trabajo para los responsables de cada una</em> </span></span></span><em><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">de las fases de la gestión del ciclo del proyecto</span></span></span></em></li>
 	<li style="text-align:left;"><em><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">Las propuestas no requiriesen para su aplicación un alto nivel de especialización sobre cuestiones ambientales.</span></span></span></em></li>
</ol>
<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">En el <strong>segundo apartado</strong> pasa a la parte práctica de integrar el medio ambiente a <strong>nivel operativo</strong>, principalmente enfocado a proyectos específicos. Se centra en la parte de planificación y el manual dispone de varias preguntas de chequeo que pueden ayudar incluir el medio ambiente en los proyectos; desde la identificación a la evaluación y el seguimiento.</span></span></span>

<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">En el </span></span></span><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"><strong>primer apartado</strong></span></span></span><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"> el manual propone un modelo</span></span></span><strong><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"> SABA</span></span></span></strong><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"> (Suelo, Agua, Biodiversidad y Atmósfera), que habría que tener en cuenta en los proyectos de cooperación y proporciona buenas prácticas para implementar en los proyectos; además de proporcionar varios ejemplos prácticos de cada uno de ellos.</span></span></span>

<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">El</span></span></span><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"><strong> tercer apartado</strong></span></span></span><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"> se centra en proponer nuevas herramientas </span></span></span><em><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">(Matriz Implicados-Recursos Naturales, Diagrama Implicados-Recursos Naturales, Matriz actividad económica-Recursos Naturales, Matriz de riesgos ambientales, Líneas de tendencia, entre otras)</span></span></span></em><span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;"> para facilitar la integración en los proyectos de la variable ambiental.</span></span></span>

<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">El manual es una herramienta interesante para incluir la variable ambiental dentro de proyectos que funcionen con la lógica del "Marco Lógico", ya que no es fácil encontrar la manera de hacerlo de una forma transversal. Es un buen manual para que las ONGD comiencen a preocuparse por el medio ambiente, y los problemas que suponen para el desarrollo.</span></span></span>

<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">No obstante, habrá que ver la evolución de los proyectos en los años venideros, para ver si se consigue una integración real de la variable ambiental, ya que a mí me parece que el "Marco Lógico" es todavía una <strong>herramienta poco flexible para</strong> la transversalización de variables como el género o el medio ambiente en los proyectos.  
</span></span></span>


<span style="color:#333333;"><span style="font-family:Ubuntu;"><span style="font-size:medium;">Más Info:  
</span></span></span>
- [Integración del medio ambiente en  políticas de cooperación al desarrollo descentralizadas](https://github.com/IzaroBlog/IzaroBlog.github.io/raw/main/_materials/IntegracionMAenCooperacion.pdf)  
- [Manual para la integración del medio ambiente en proyectos de desarrollo](https://github.com/IzaroBlog/IzaroBlog.github.io/raw/main/_materials/manualintegracionipade.pdf)  

